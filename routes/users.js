const express = require('express');
const path = require('path');
const fs = require('fs');

const usersPath = path.join(__dirname, '..', 'data', 'users.json');
const users = JSON.parse(fs.readFileSync(usersPath, 'utf8'));

const router = express.Router();

router.get('/', (req, res) => res.send(users));

router.get('/:id', (req, res) => {
  // eslint-disable-next-line no-underscore-dangle
  const user = users.find((element) => element._id === req.params.id);
  if (!user) {
    res.status(404).send({ message: 'Нет пользователя с таким id' });
    return;
  }
  res.send(user);
});

module.exports = router;
